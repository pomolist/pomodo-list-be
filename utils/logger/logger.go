package utils

import (
	"os"

	"github.com/sirupsen/logrus"
)

var Logger *logrus.Logger

func init() {
	Logger = logrus.New()
	Logger.SetOutput(os.Stdout)
	Logger.SetLevel(getLogLevel("info"))
}

func getLogLevel(level string) logrus.Level {

	switch level {
	case "warn":
		return logrus.WarnLevel
	case "error":
		return logrus.ErrorLevel
	case "trace":
		return logrus.TraceLevel
	case "panic":
		return logrus.PanicLevel
	case "debug":
		return logrus.DebugLevel
	}
	return logrus.InfoLevel
}
