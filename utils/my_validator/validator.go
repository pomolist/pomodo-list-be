package myvalidator

import (
	"net/http"
	"pomodo-list-be/utils/errors"
	"pomodo-list-be/utils/responser"
	"reflect"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type CustomValidator struct {
	Validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.Validator.Struct(i); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return nil
}

func ValidateRequest(c echo.Context, t interface{}) (error, res interface{}) {
	valueType := reflect.TypeOf(t)
	if valueType.Kind() != reflect.Ptr {
		responser.ErrorWithCode(c, http.StatusBadRequest, "param t must be a pointer", errors.ErrorCodeInternalServerError)
		return nil, nil
	}
	newValue := reflect.New(valueType.Elem()).Interface()
	if err := c.Bind(newValue); err != nil {
		responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
		return nil, nil
	}
	if err := c.Validate(newValue); err != nil {
		responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
		return nil, nil
	}

	return nil, newValue
}
