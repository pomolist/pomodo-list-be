package responser

import (
	"github.com/labstack/echo/v4"
)

// Success ...
func Success(c echo.Context, status int, data interface{}) error {
	return c.JSON(status, echo.Map{
		"status":     status,
		"error_code": 0,
		"message":    "Success",
		"data":       data,
	})
}

func Error(c echo.Context, status int, message string) error {
	return c.JSON(status, echo.Map{
		"status":     status,
		"error_code": 0,
		"message":    message,
		"data":       nil,
	})

}

func ErrorWithCode(c echo.Context, status int, message string, errorCode int) error {
	return c.JSON(status, echo.Map{
		"status":     status,
		"error_code": errorCode,
		"message":    message,
		"data":       nil,
	})
}
