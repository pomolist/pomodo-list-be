package errors

const (
	ErrorCodeNoError             = 0
	ErrorCodeEmailExisted        = 100
	ErrorCodeUserNotFound        = 101
	ErrorCodePasswordIncorrect   = 102
	ErrorCodeInternalServerError = 403
	ErrorCodeUnauthorized        = 401
	ErrorCodeValidation          = 409
	ErrorCodeUserAlreadyLogin    = 103
)

var ErrorMessage = map[int]string{
	ErrorCodeEmailExisted:        "Email already existed, please try another email!",
	ErrorCodeUserNotFound:        "User not found!",
	ErrorCodePasswordIncorrect:   "Password is incorrect!",
	ErrorCodeInternalServerError: "Internal server error!",
	ErrorCodeUnauthorized:        "Unauthorized!",
	ErrorCodeValidation:          "Validation error!",
	ErrorCodeUserAlreadyLogin:    "User already login!",
}
