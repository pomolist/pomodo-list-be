package service

import (
	"errors"
	"pomodo-list-be/internal/app/api/request"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/repositories"
	utils "pomodo-list-be/utils/logger"

	"time"

	"gorm.io/gorm"
)

type TaskService struct {
	repository             repositories.TaskRepository
	taskCategoryRepository repositories.TaskCategoryRepository
	categoryRepository     repositories.CategoryRepository
}

func NewTaskService(repository *repositories.TaskRepository, taskCategoryRepository *repositories.TaskCategoryRepository, categoryRepository *repositories.CategoryRepository) *TaskService {
	return &TaskService{repository: *repository, taskCategoryRepository: *taskCategoryRepository, categoryRepository: *categoryRepository}
}

func (taskService *TaskService) Create(user model.AuthUser, req request.CreateTaskRequest, tx *gorm.DB) (*model.Task, error) {

	task := model.Task{
		Title:       req.Title,
		Description: req.Description,
		DueDate:     time.Unix(int64(req.DueDate), 0),
		UserID:      user.ID,
	}
	return taskService.repository.Create(&task, tx)
}

func (taskService *TaskService) CreateTaskCategories(user *model.AuthUser, req *request.CreateTaskRequest, task *model.Task, tx *gorm.DB) ([]*model.TaskCategory, error) {

	categories, err := taskService.categoryRepository.GetCategories(req.CategoryIds)
	if err != nil || len(categories) == 0 {
		return nil, err
	}
	taskCategories := make([]*model.TaskCategory, 0)
	for _, category := range categories {
		taskCategory := &model.TaskCategory{
			TaskID:     task.ID,
			CategoryID: category.ID,
		}
		taskCategories = append(taskCategories, taskCategory)
	}
	return taskService.taskCategoryRepository.InserMany(taskCategories, tx)
}

func (taskService *TaskService) GetTasks(userId uint) ([]*model.TaskReponse, error) {
	tasks, err := taskService.repository.GetTasksByUserId(userId)
	if err != nil {
		return make([]*model.TaskReponse, 0), err
	}
	taskMap := make(map[uint]*model.TaskReponse)
	for _, task := range tasks {
		if _, ok := taskMap[task.ID]; !ok {
			taskMap[task.ID] = task
		} else {
			taskMap[task.ID].Categories = append(taskMap[task.ID].Categories, task.Categories...)
		}
	}

	taskResponses := make([]*model.TaskReponse, 0)

	for _, task := range taskMap {
		taskResponses = append(taskResponses, task)
	}
	return taskResponses, nil
}

func (taskService *TaskService) Delete(user *model.AuthUser, taskId uint) error {
	task, err := taskService.repository.GetTaskById(taskId)
	if err != nil {
		return err
	}
	if task.UserID != user.ID || task.Status == model.TaskStatusDeleted {
		return errors.New("task not found")
	}
	return taskService.repository.Delete(taskId)
}

func (taskService *TaskService) Update(user *model.AuthUser, req request.UpdateTaskRequest, tx *gorm.DB) (*model.Task, error) {
	task, err := taskService.repository.GetTaskById(req.ID)
	if err != nil {
		return nil, err
	}
	if task.UserID != user.ID || task.Status == model.TaskStatusDeleted {
		return nil, errors.New("task not found")
	}
	task.Title = req.Title
	task.Description = req.Description
	task.DueDate = time.Unix(int64(req.DueDate), 0)
	task.Status = req.Status
	return taskService.repository.Update(&task, tx)
}

func (taskService *TaskService) UpdateTaskCategories(req *request.UpdateTaskRequest, task *model.Task, tx *gorm.DB) ([]*model.TaskCategory, error) {
	err := taskService.taskCategoryRepository.DeleteByTaskId(task.ID, tx)
	if err != nil {
		return nil, err
	}
	categories, err := taskService.categoryRepository.GetCategories(req.CategoryIds)
	if err != nil {
		return nil, err
	}
	taskCategories := make([]*model.TaskCategory, 0)
	for _, category := range categories {
		taskCategory := &model.TaskCategory{
			TaskID:     task.ID,
			CategoryID: category.ID,
		}
		taskCategories = append(taskCategories, taskCategory)
	}
	return taskService.taskCategoryRepository.InserMany(taskCategories, tx)
}

func (taskService *TaskService) GetCategoriesByTaskId(taskId uint) ([]model.CategoryResponse, error) {
	taskCategories, err := taskService.taskCategoryRepository.GetTaskCategoriesByTaskId(taskId)
	utils.Logger.Info(taskCategories)
	if err != nil {
		return nil, err
	}
	return taskCategories, nil
}
