package service

import (
	"fmt"
	"pomodo-list-be/internal/app/api/request"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/repositories"

	"pomodo-list-be/utils/errors"
	utils "pomodo-list-be/utils/logger"

	"time"

	"golang.org/x/crypto/bcrypt"
)

type AuthService struct {
	repository   repositories.UserRepository
	tokenService TokenService
}

func NewAuthService(repository *repositories.UserRepository, tokenService *TokenService) *AuthService {
	return &AuthService{repository: *repository, tokenService: *tokenService}
}

func (auth *AuthService) Signup(req request.SignUpRequest) (*model.User, int) {
	userExisted, err := auth.repository.FindByEmail(req.Email)
	utils.Logger.Info(userExisted)
	fmt.Println(err)
	if err != nil && err.Error() != "record not found" {
		return &model.User{}, errors.ErrorCodeInternalServerError
	}
	if userExisted.ID != 0 {
		return &model.User{}, errors.ErrorCodeEmailExisted
	}
	pass, errr := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if errr != nil {
		return &model.User{}, errors.ErrorCodeInternalServerError
	}

	user, err := auth.repository.CreateUser(&model.User{
		Email:    req.Email,
		Fullname: req.Fullname,
		Password: string(pass),
	})
	if err != nil {
		return &model.User{}, errors.ErrorCodeInternalServerError
	}
	return user, errors.ErrorCodeNoError
}

func (auth *AuthService) Login(req request.LoginRequest) (*model.User, int) {
	user, err := auth.repository.FindByEmail(req.Email)
	if err != nil {
		return &model.User{}, errors.ErrorCodeUserNotFound
	}
	if !user.ComparePassword(req.Password) {
		return &model.User{}, errors.ErrorCodePasswordIncorrect
	}

	userToken, err := auth.tokenService.repository.Find(&model.UserToken{
		UserID:   user.ID,
		DeviceID: req.DeviceId,
	})
	if err != nil && err.Error() != "record not found" {
		return &model.User{}, errors.ErrorCodeInternalServerError
	}
	utils.Logger.Info(userToken.ExpiredAt)
	utils.Logger.Info(time.Now().UTC())
	utils.Logger.Info(userToken.ExpiredAt.Before(time.Now().UTC()))

	if userToken.ExpiredAt.Before(time.Now().UTC()) {
		utils.Logger.Info("expired")
		auth.tokenService.repository.Delete(userToken)
	} else if userToken.ID != 0 {
		return &model.User{}, errors.ErrorCodeUserAlreadyLogin
	}

	return user, errors.ErrorCodeNoError
}
