package service

import (
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/repositories"
)

type UserService struct {
	repository repositories.UserRepository
}

func NewUserService(repository *repositories.UserRepository) *UserService {
	return &UserService{repository: *repository}
}

func (_this *UserService) GetUserById(id uint) (model.User, error) {
	user, errCode := _this.repository.FindById(id)
	return *user, errCode
}
