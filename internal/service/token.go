package service

import (
	"crypto/rand"
	"fmt"
	"os"
	"pomodo-list-be/internal/app/api/request"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/repositories"
	"pomodo-list-be/utils/errors"

	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type TokenService struct {
	repository     repositories.UserTokenRepository
	userRepository repositories.UserRepository
}

func NewTokenService(repository *repositories.UserTokenRepository, userRepository *repositories.UserRepository) *TokenService {
	return &TokenService{repository: *repository, userRepository: *userRepository}
}
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
func (_this *TokenService) GenerateToken(user *model.User, deviceId string) (model.UserToken, error) {

	second, err := strconv.Atoi(os.Getenv("JWT_EXPIRE"))
	expired := time.Now().UTC().Add(time.Second * time.Duration(second))

	if err != nil {
		return model.UserToken{}, err
	}

	claims := model.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "pomodo-list",
			ExpiresAt: jwt.NewNumericDate(expired),
		},
		Id:       user.ID,
		Email:    user.Email,
		Fullname: user.Fullname,
		Status:   user.Status,
	}

	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(os.Getenv("JWT_SECRET")))

	if err != nil {
		return model.UserToken{}, err
	}
	refreshToken := make([]byte, 20)
	_, err = rand.Read(refreshToken)

	if err != nil {
		return model.UserToken{}, err
	}

	token := model.UserToken{
		UserID:       user.ID,
		DeviceID:     deviceId,
		AccessToken:  accessToken,
		RefreshToken: fmt.Sprintf("%x", refreshToken),
		ExpiredAt:    expired,
	}
	return token, nil
}

func (_this *TokenService) SaveToken(token model.UserToken) (*model.UserToken, error) {
	return _this.repository.CreateToken(&token)
}

func (_this *TokenService) RefreshToken(req *request.RefreshTokenRequest) (*model.UserToken, int) {
	token, err := _this.repository.Find(&model.UserToken{RefreshToken: req.RefreshToken, DeviceID: req.DeviceID})

	emptyUserToken := &model.UserToken{}
	if err != nil && err.Error() != "record not found" {
		return emptyUserToken, errors.ErrorCodeInternalServerError
	}

	if err != nil && err.Error() == "record not found" {
		return emptyUserToken, errors.ErrorCodeUnauthorized
	}

	if token.DeviceID != req.DeviceID || token.ExpiredAt.Before(time.Now()) {
		return emptyUserToken, errors.ErrorCodeUnauthorized
	}

	user, err := _this.userRepository.FindById(token.UserID)

	if err != nil {
		return emptyUserToken, errors.ErrorCodeUserNotFound
	}

	newToken, err := _this.GenerateToken(user, req.DeviceID)

	if err != nil {
		return emptyUserToken, errors.ErrorCodeInternalServerError
	}

	err = _this.repository.DeleteToken(token)

	if err != nil {
		return emptyUserToken, errors.ErrorCodeInternalServerError
	}

	tokenResponse, Error := _this.repository.CreateToken(&newToken)

	if Error != nil {
		return emptyUserToken, errors.ErrorCodeInternalServerError
	}

	return tokenResponse, errors.ErrorCodeNoError

}

func (_this *TokenService) Logout(userId uint) error {
	return _this.repository.DeleteByUserId(userId)
}

func (_this *TokenService) ValidateToken(token string) (model.AuthUser, int) {
	t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_SECRET")), nil
	})

	if err != nil {
		return model.AuthUser{}, errors.ErrorCodeUnauthorized
	}
	if claims, ok := t.Claims.(jwt.MapClaims); ok && t.Valid {
		if claims["id"] == nil {
			return model.AuthUser{}, errors.ErrorCodeUnauthorized
		}
		user := model.AuthUser{
			ID:       uint(claims["id"].(float64)),
			Email:    claims["email"].(string),
			Fullname: claims["fullname"].(string),
		}

		return user, errors.ErrorCodeNoError

	} else {
		return model.AuthUser{}, errors.ErrorCodeUnauthorized
	}
}
