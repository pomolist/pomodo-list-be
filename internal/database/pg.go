package database

import (
	"fmt"
	"log"
	"os"
	"pomodo-list-be/internal/model"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type PgClient struct {
	Client *gorm.DB
}

func (client *PgClient) Connect() {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			ParameterizedQueries:      true,        // Don't include params in the SQL log
			Colorful:                  false,       // Disable color
		},
	)
	db, err := gorm.Open(postgres.Open(os.Getenv("DATABASE_URI")), &gorm.Config{
		Logger:                 newLogger,
		SkipDefaultTransaction: true,
	})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&model.User{})
	db.AutoMigrate(&model.UserToken{})
	db.AutoMigrate(&model.Category{})
	db.AutoMigrate(&model.Task{})
	db.AutoMigrate(&model.TaskCategory{})

	fmt.Println("Connected to database")

	client.Client = db

}

func (client *PgClient) Close() {

}
