package model

import "time"

type TaskCategory struct {
	TaskID     uint      `gorm:"primaryKey"`
	CategoryID uint      `gorm:"primaryKey"`
	CreatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP"`

	Category Category `gorm:"foreignKey:CategoryID"`
}
