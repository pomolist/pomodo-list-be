package model

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type Claims struct {
	jwt.RegisteredClaims
	Id       uint   `json:"id"`
	Email    string `json:"email"`
	Fullname string `json:"fullname"`
	Status   uint   `json:"status"`
}

type UserToken struct {
	ID           uint      `gorm:"primaryKey"`
	UserID       uint      `gorm:"not nulll;UNIQUE_INDEX:idx_user_id_device_id"`
	AccessToken  string    `gorm:"not null"`
	RefreshToken string    `gorm:"not null"`
	DeviceID     string    `gorm:"not:null;UNIQUE_INDEX:idx_user_id_device_id"`
	ExpiredAt    time.Time `gorm:"not null"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP"`
}

type UserTokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiredAt    int64  `json:"expired_at"`
}

func (userToken *UserToken) Transform() UserTokenResponse {
	return UserTokenResponse{
		AccessToken:  userToken.AccessToken,
		RefreshToken: userToken.RefreshToken,
		ExpiredAt:    userToken.ExpiredAt.Unix(),
	}
}
