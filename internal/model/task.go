package model

import "time"

// enum taskstatus
const (
	TaskStatusPending  = 1
	TaskStatusProgress = 2
	TaskStatusDone     = 3
	TaskStatusDeleted  = 4
)

type Task struct {
	ID          uint       `gorm:"primaryKey"`
	UserID      uint       `gorm:"not null"`
	Title       string     `gorm:"not null"`
	Description string     `gorm:"default:null"`
	DueDate     time.Time  `gorm:"default:CURRENT_TIMESTAMP"`
	Status      uint       `gorm:"default:1"`
	CreatedAt   time.Time  `gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt   time.Time  `gorm:"default:CURRENT_TIMESTAMP"`
	Categories  []Category `gorm:"many2many:task_categories;"`
}

type TaskModelReponse struct {
	ID           uint      `json:"id"`
	Title        string    `json:"title"`
	Description  string    `json:"description"`
	DueDate      time.Time `json:"due_date"`
	CategoryID   uint      `json:"category_id"`
	CategoryName string    `json:"category_name"`
}

type TaskReponse struct {
	ID          uint               `json:"id"`
	Title       string             `json:"title"`
	Description string             `json:"description"`
	DueDate     uint               `json:"due_date"`
	Categories  []CategoryResponse `json:"categories"`
}

func (taskReponse *TaskModelReponse) Transform() *TaskReponse {
	return &TaskReponse{
		ID:          taskReponse.ID,
		Title:       taskReponse.Title,
		Description: taskReponse.Description,
		DueDate:     uint(taskReponse.DueDate.Unix()),
		Categories: []CategoryResponse{
			{
				ID:           taskReponse.CategoryID,
				CategoryName: taskReponse.CategoryName,
			},
		},
	}
}
