package model

import "time"

type Category struct {
	ID           uint      `gorm:"primaryKey"`
	CategoryName string    `gorm:"not null;unique"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP"`
	Tasks        []Task    `gorm:"many2many:task_categories;"`
}

type CategoryResponse struct {
	ID           uint   `json:"id"`
	CategoryName string `json:"category_name"`
}
