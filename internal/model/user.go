package model

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        uint        `gorm:"primaryKey" json:"id"`
	Fullname  string      `gorm:"not null" json:"fullname"`
	Email     string      `gorm:"not null;unique" json:"email"`
	Password  string      `gorm:"not null" json:"-"`
	Status    uint        `gorm:"not null;default:1" json:"status"`
	Avatar    string      `gorm:"default:null" json:"avatar"`
	CreatedAt time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Tokens    []UserToken `gorm:"foreignKey:UserID" json:"-"`
	Tasks     []Task      `gorm:"foreignKey:UserID" json:"tasks"`
}

type UserResponse struct {
	Id       uint   `json:"id"`
	Email    string `json:"email"`
	Fullname string `json:"fullname"`
	Status   uint   `json:"status"`
	Avatar   string `json:"avatar"`
}

type AuthUser struct {
	ID       uint   `json:"id"`
	Email    string `json:"email"`
	Fullname string `json:"fullname"`
	Status   int    `json:"status"`
}

func (user *User) Transform() UserResponse {
	return UserResponse{
		Id:       (user.ID),
		Email:    user.Email,
		Fullname: user.Fullname,
		Status:   user.Status,
		Avatar:   user.Avatar,
	}
}

func (user *User) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) == nil
}
