package controller

import (
	"net/http"
	"pomodo-list-be/internal/app/api/request"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/service"
	"pomodo-list-be/utils/errors"
	utils "pomodo-list-be/utils/logger"
	myvalidator "pomodo-list-be/utils/my_validator"
	"pomodo-list-be/utils/responser"

	"github.com/labstack/echo/v4"
)

type AuthController struct {
	AuthService  service.AuthService
	TokenService service.TokenService
}

func NewAuthController(authService *service.AuthService, tokenService *service.TokenService) *AuthController {
	return &AuthController{AuthService: *authService, TokenService: *tokenService}
}

func (auth *AuthController) Login(c echo.Context) error {
	_, resVal := myvalidator.ValidateRequest(c, &request.LoginRequest{})

	req := resVal.(*request.LoginRequest)

	user, errCode := auth.AuthService.Login(*req)

	if errCode != 0 {
		responser.ErrorWithCode(c, http.StatusBadRequest, errors.ErrorMessage[errCode], errCode)
		return nil
	}

	token, err := auth.TokenService.GenerateToken(user, req.DeviceId)

	if err != nil {
		responser.Error(c, http.StatusInternalServerError, err.Error())
		return nil
	}

	res, err := auth.TokenService.SaveToken(token)

	if err != nil {
		responser.Error(c, http.StatusInternalServerError, err.Error())
		return nil
	}

	responser.Success(c, http.StatusOK, echo.Map{
		"user":  user.Transform(),
		"token": res.Transform(),
	})

	return nil

}

func (authCtrl *AuthController) Signup(c echo.Context) error {
	_, resVal := myvalidator.ValidateRequest(c, &request.SignUpRequest{})

	req := resVal.(*request.SignUpRequest)

	user, errCode := authCtrl.AuthService.Signup(*req)

	if errCode != 0 {
		responser.ErrorWithCode(c, http.StatusBadRequest, errors.ErrorMessage[errCode], errCode)
	}
	responser.Success(c, http.StatusOK, user.Transform())
	return nil
}

func (authCtrl *AuthController) RefreshToken(c echo.Context) error {
	_, resVal := myvalidator.ValidateRequest(c, &request.RefreshTokenRequest{})

	req := resVal.(*request.RefreshTokenRequest)

	token, errCode := authCtrl.TokenService.RefreshToken(req)

	utils.Logger.Info(errCode)

	if errCode != 0 {
		responser.ErrorWithCode(c, http.StatusBadRequest, errors.ErrorMessage[errCode], errCode)
		return nil
	}

	responser.Success(c, http.StatusOK, echo.Map{
		"token": token.Transform(),
	})

	return nil
}

func (authCtrl *AuthController) Logout(c echo.Context) error {

	user := c.Get("user")

	err := authCtrl.TokenService.Logout(user.(model.AuthUser).ID)
	if err != nil {
		responser.Error(c, http.StatusBadRequest, err.Error())
		return nil
	}
	responser.Success(c, http.StatusOK, nil)
	return nil
}
