package controller

import (
	"net/http"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/service"
	"pomodo-list-be/utils/errors"
	"pomodo-list-be/utils/responser"

	"github.com/labstack/echo/v4"
)

type UserController struct {
	UserService service.UserService
}

func NewUserController(UserService *service.UserService) *UserController {
	return &UserController{UserService: *UserService}
}

func (userCtrl *UserController) GetUserMe(c echo.Context) error {
	user := c.Get("user").(model.AuthUser)
	userData, err := userCtrl.UserService.GetUserById(user.ID)

	if err != nil {
		responser.ErrorWithCode(c, http.StatusBadRequest, errors.ErrorMessage[errors.ErrorCodeUserNotFound], errors.ErrorCodeUserNotFound)
		return nil
	}
	responser.Success(c, http.StatusOK, userData.Transform())
	return nil
}
