package controller

import (
	"net/http"
	"pomodo-list-be/internal/app/api/request"
	"pomodo-list-be/internal/model"
	"pomodo-list-be/internal/service"
	"pomodo-list-be/utils/errors"
	myvalidator "pomodo-list-be/utils/my_validator"
	"pomodo-list-be/utils/responser"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type TaskController struct {
	taskService service.TaskService
	pgClient    *gorm.DB
}

func NewTaskController(TaskService *service.TaskService, pgClient *gorm.DB) *TaskController {
	return &TaskController{taskService: *TaskService, pgClient: pgClient}
}

func (taskCtrl *TaskController) Create(c echo.Context) error {

	_, resVal := myvalidator.ValidateRequest(c, &request.CreateTaskRequest{})

	user := c.Get("user").(model.AuthUser)

	req := resVal.(*request.CreateTaskRequest)

	err := taskCtrl.pgClient.Transaction(func(tx *gorm.DB) error {
		task, err := taskCtrl.taskService.Create(user, *req, tx)
		if err != nil {
			responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
			return err
		}

		taskCategories, err := taskCtrl.taskService.CreateTaskCategories(&user, req, task, tx)

		categories := make([]uint, 0)

		for _, taskCategory := range taskCategories {
			categories = append(categories, taskCategory.CategoryID)
		}

		if err != nil {
			responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
			return err
		}

		responser.Success(c, http.StatusOK, echo.Map{
			"id":          task.ID,
			"title":       task.Title,
			"description": task.Description,
			"due_date":    task.DueDate,
			"categories":  categories,
		})
		return nil
	})

	return err

}

func (taskCtrl *TaskController) GetTasks(c echo.Context) error {

	user := c.Get("user").(model.AuthUser)

	tasks, err := taskCtrl.taskService.GetTasks(user.ID)

	if err != nil {
		responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
		return nil
	}

	responser.Success(c, http.StatusOK, tasks)
	return nil
}

func (taskCtrl *TaskController) Delete(c echo.Context) error {

	_, resVal := myvalidator.ValidateRequest(c, &request.DeleteTaskRequest{})
	req := resVal.(*request.DeleteTaskRequest)

	user := c.Get("user").(model.AuthUser)

	err := taskCtrl.taskService.Delete(&user, req.ID)

	if err != nil {
		responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
		return nil
	}

	responser.Success(c, http.StatusOK, nil)

	return nil
}

func (taskCtrl *TaskController) Update(c echo.Context) error {

	_, resVal := myvalidator.ValidateRequest(c, &request.UpdateTaskRequest{})
	req := resVal.(*request.UpdateTaskRequest)

	user := c.Get("user").(model.AuthUser)

	err := taskCtrl.pgClient.Transaction(func(tx *gorm.DB) error {
		task, err := taskCtrl.taskService.Update(&user, *req, tx)
		if err != nil {
			responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
			return err
		}

		if len(req.CategoryIds) != 0 {
			_, err := taskCtrl.taskService.UpdateTaskCategories(req, task, tx)
			if err != nil {
				responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
				return err
			}
		}

		categories, err := taskCtrl.taskService.GetCategoriesByTaskId(task.ID)

		if err != nil {
			responser.ErrorWithCode(c, http.StatusBadRequest, err.Error(), errors.ErrorCodeInternalServerError)
			return err
		}

		responser.Success(c, http.StatusOK, echo.Map{
			"id":          task.ID,
			"title":       task.Title,
			"description": task.Description,
			"due_date":    task.DueDate,
			"categories":  categories,
		})
		return nil
	})

	return err

}
