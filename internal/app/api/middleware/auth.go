package middleware

import (
	"net/http"
	"pomodo-list-be/internal/service"
	"pomodo-list-be/utils/errors"
	"pomodo-list-be/utils/responser"

	"strings"

	"github.com/labstack/echo/v4"
)

type AuthMiddleware struct{}

func NewAuthMiddleware() *AuthMiddleware {
	return &AuthMiddleware{}
}

func (auth *AuthMiddleware) Process(service *service.TokenService) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if c.Request().Header.Get("Authorization") == "" {
				responser.ErrorWithCode(c, http.StatusUnauthorized, "Unauthorized", errors.ErrorCodeUnauthorized)
				return nil
			}

			token := strings.Split(c.Request().Header.Get("Authorization"), "Bearer ")[1]

			if token == "" {
				responser.ErrorWithCode(c, http.StatusUnauthorized, "Unauthorized", errors.ErrorCodeUnauthorized)
				return nil
			}
			userData, errCode := service.ValidateToken(token)
			if errCode != 0 {
				responser.ErrorWithCode(c, http.StatusUnauthorized, "Unauthorized", errors.ErrorCodeUnauthorized)
				return nil
			}
			c.Set("user", userData)
			return next(c)
		}
	}
}
