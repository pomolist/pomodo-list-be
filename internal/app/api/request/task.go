package request

type CreateTaskRequest struct {
	Title       string `json:"title" validate:"required,min=3,max=250"`
	Description string `json:"description" validate:"max=500"`
	DueDate     int    `json:"due_date" validate:"required"`
	CategoryIds []uint `json:"category_ids"`
}

type DeleteTaskRequest struct {
	ID uint `json:"id" validate:"required"`
}

type UpdateTaskRequest struct {
	ID          uint   `json:"id" validate:"required"`
	Title       string `json:"title" validate:"max=250"`
	Description string `json:"description" validate:"max=500"`
	DueDate     int    `json:"due_date"`
	Status      uint   `json:"status"`
	CategoryIds []uint `json:"category_ids"`
}
