package repositories

import (
	"fmt"
	"pomodo-list-be/internal/model"

	"gorm.io/gorm"
)

type UserTokenRepository struct {
	pgClient *gorm.DB
}

func NewUserTokenRepository(pgClient *gorm.DB) *UserTokenRepository {
	return &UserTokenRepository{pgClient: pgClient}
}

func (userToken *UserTokenRepository) CreateToken(token *model.UserToken) (*model.UserToken, error) {
	return token, userToken.pgClient.Create(token).Error
}

func (userToken *UserTokenRepository) DeleteToken(token *model.UserToken) error {
	return userToken.pgClient.Delete(token).Error
}

func (userToken *UserTokenRepository) FindByToken(token string) (*model.UserToken, error) {
	tokenResponse := model.UserToken{
		RefreshToken: token,
	}
	res := userToken.pgClient.First(&tokenResponse)
	fmt.Println(res)
	return &tokenResponse, res.Error
}

func (userToken *UserTokenRepository) Find(token *model.UserToken) (*model.UserToken, error) {
	res := userToken.pgClient.Debug().Where(&token).First(&token)
	return token, res.Error
}

func (userToken *UserTokenRepository) DeleteByUserId(userId uint) error {
	return userToken.pgClient.Debug().Where(&model.UserToken{UserID: userId}).Delete(&model.UserToken{}).Error
}

func (userToken *UserTokenRepository) Delete(token *model.UserToken) error {
	return userToken.pgClient.Debug().Delete(&token).Error
}
