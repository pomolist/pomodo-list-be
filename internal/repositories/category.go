package repositories

import (
	"pomodo-list-be/internal/model"

	"gorm.io/gorm"
)

type CategoryRepository struct {
	pgClient *gorm.DB
}

func NewCategoryRepository(pgClient *gorm.DB) *CategoryRepository {
	return &CategoryRepository{pgClient: pgClient}
}

func (categoryRes *CategoryRepository) GetCategories(ids []uint) ([]*model.Category, error) {
	var categories []*model.Category
	err := categoryRes.pgClient.Where("id IN ?", ids).Find(&categories).Error
	return categories, err
}

func (categoryRes *CategoryRepository) GetCategoriesByTaskId(taskId uint) ([]*model.Category, error) {

	var categories []*model.Category

	categoryRes.pgClient.Where("task_id = ?", taskId).Find(&categories)

	return categories, nil
}
