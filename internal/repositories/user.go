package repositories

import (
	"pomodo-list-be/internal/model"

	"gorm.io/gorm"
)

type UserRepository struct {
	pgClient *gorm.DB
}

func NewUserRepository(pgClient *gorm.DB) *UserRepository {
	return &UserRepository{pgClient: pgClient}
}

func (userRes *UserRepository) FindByEmail(email string) (*model.User, error) {
	var user model.User
	return &user, userRes.pgClient.Where("email = ?", email).First(&user).Error
}

func (userRes *UserRepository) CreateUser(user *model.User) (*model.User, error) {
	return user, userRes.pgClient.Create(&user).Error

}

func (userRes *UserRepository) FindById(id uint) (*model.User, error) {
	var user model.User
	return &user, userRes.pgClient.First(&user, id).Error
}
