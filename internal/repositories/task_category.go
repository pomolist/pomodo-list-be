package repositories

import (
	"pomodo-list-be/internal/model"
	utils "pomodo-list-be/utils/logger"

	"gorm.io/gorm"
)

type TaskCategoryRepository struct {
	pgClient *gorm.DB
}

func NewTaskCategoryRepository(pgClient *gorm.DB) *TaskCategoryRepository {
	return &TaskCategoryRepository{pgClient: pgClient}
}

func (taskCategoryRes *TaskCategoryRepository) InserMany(taskCategories []*model.TaskCategory, tx *gorm.DB) ([]*model.TaskCategory, error) {
	if tx != nil {
		return taskCategories, tx.Create(&taskCategories).Error
	}
	return taskCategories, taskCategoryRes.pgClient.Create(&taskCategories).Error
}

func (taskcategoryRes *TaskCategoryRepository) Create(taskCategory *model.TaskCategory) (*model.TaskCategory, error) {
	return taskCategory, taskcategoryRes.pgClient.Create(&taskCategory).Error
}

func (taskcategoryRes *TaskCategoryRepository) GetTaskCategoriesByTaskId(taskId uint) ([]model.CategoryResponse, error) {
	var taskCategories []model.CategoryResponse
	taskcategoryRes.pgClient.Raw(`
        SELECT categories.* FROM task_categories 
        JOIN categories ON task_categories.category_id = categories.id AND task_id = ?
    `, taskId).Scan(&taskCategories)
	utils.Logger.Info(&taskCategories)
	return taskCategories, nil
}

func (taskcategoryRes *TaskCategoryRepository) DeleteByTaskId(taskId uint, tx *gorm.DB) error {
	return taskcategoryRes.pgClient.Where("task_id = ?", taskId).Delete(&model.TaskCategory{}).Error
}
