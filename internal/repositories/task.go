package repositories

import (
	"pomodo-list-be/internal/model"

	"gorm.io/gorm"
)

type TaskRepository struct {
	pgClient *gorm.DB
}

func NewTaskRepository(pgClient *gorm.DB) *TaskRepository {
	return &TaskRepository{pgClient: pgClient}

}

func (taskRes *TaskRepository) Create(task *model.Task, tx *gorm.DB) (*model.Task, error) {
	if tx != nil {
		return task, tx.Create(&task).Error
	}
	return task, taskRes.pgClient.Create(&task).Error
}

func (taskRes *TaskRepository) GetTaskById(taskId uint) (model.Task, error) {

	var task model.Task

	err := taskRes.pgClient.Where("id = ?", taskId).First(&task).Error

	return task, err

}

func (taskRes *TaskRepository) GetTasksByUserId(userId uint) ([]*model.TaskReponse, error) {

	var tasks []model.TaskModelReponse

	taskRes.pgClient.Raw(`
        SELECT t.*, c.category_name as category_name, c.id as category_id
        FROM tasks as t
        JOIN task_categories as tc ON t.id = tc.task_id
        JOIN categories as c ON c.id = tc.category_id
        WHERE t.user_id = ?
        GROUP BY t.id, c.id
    `, userId).Scan(&tasks)

	taskResponses := make([]*model.TaskReponse, 0)

	for _, task := range tasks {
		taskResponses = append(taskResponses, task.Transform())
	}

	return taskResponses, nil
}

func (taskRes *TaskRepository) Delete(taskId uint) error {
	task := model.Task{ID: taskId}
	return taskRes.pgClient.Model(&task).Where("id = ?", taskId).Update("status", model.TaskStatusDeleted).Error
}

func (taskRes *TaskRepository) Update(task *model.Task, tx *gorm.DB) (*model.Task, error) {
	if tx != nil {
		return task, tx.Model(&task).Updates(&task).Error
	}
	return task, taskRes.pgClient.Model(&task).Updates(&task).Error
}
