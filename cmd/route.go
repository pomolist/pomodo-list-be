package main

import (
	"github.com/labstack/echo/v4"
)

func RegisterRouter(e *echo.Echo, container *Container) {
	auth := e.Group("/auth")
	auth.POST("/signup", container.AuthController.Signup)
	auth.POST("/login", container.AuthController.Login)
	auth.POST("/refresh-token", container.AuthController.RefreshToken)
	auth.POST("/logout", container.AuthController.Logout, container.AuthMiddleware.Process(container.TokenService))
	task := e.Group("/tasks", container.AuthMiddleware.Process(container.TokenService))
	task.POST("", container.TaskController.Create)
	task.GET("", container.TaskController.GetTasks)
	task.POST("/update", container.TaskController.Update)
	task.POST("/delete", container.TaskController.Delete)

}
