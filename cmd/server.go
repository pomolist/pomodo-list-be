package main

import (
	myvalidator "pomodo-list-be/utils/my_validator"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func SetupServer(c *echo.Echo) {
	c.Use(middleware.Logger())
	c.Use(middleware.Recover())

	c.Validator = &myvalidator.CustomValidator{Validator: validator.New()}

	c.Logger.Fatal(c.Start(":3000"))
}
