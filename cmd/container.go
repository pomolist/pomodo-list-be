package main

import (
	"pomodo-list-be/internal/app/api/controller"
	"pomodo-list-be/internal/app/api/middleware"
	"pomodo-list-be/internal/database"
	"pomodo-list-be/internal/repositories"
	"pomodo-list-be/internal/service"

	"gorm.io/gorm"
)

type Container struct {
	PgClient *gorm.DB

	UserRespository *repositories.UserRepository

	AuthService *service.AuthService

	TokenService *service.TokenService

	AuthController *controller.AuthController

	AuthMiddleware *middleware.AuthMiddleware

	TaskController *controller.TaskController
}

func NewContainer(PgClient *gorm.DB, UserRespository *repositories.UserRepository, AuthService *service.AuthService, AuthController *controller.AuthController, AuthMiddleware *middleware.AuthMiddleware, TokenService *service.TokenService, TaskController *controller.TaskController) *Container {

	return &Container{
		PgClient:        PgClient,
		UserRespository: UserRespository,
		AuthService:     AuthService,
		AuthController:  AuthController,
		AuthMiddleware:  AuthMiddleware,
		TokenService:    TokenService,
		TaskController:  TaskController,
	}
}

func InitializeContainer() *Container {

	PgClient := database.PgClient{}
	PgClient.Connect()

	UserRepository := repositories.NewUserRepository(PgClient.Client)
	TokenRepository := repositories.NewUserTokenRepository(PgClient.Client)
	TokenService := service.NewTokenService(TokenRepository, UserRepository)
	AuthService := service.NewAuthService(UserRepository, TokenService)

	AuthController := controller.NewAuthController(AuthService, TokenService)

	AuthMiddleware := middleware.NewAuthMiddleware()

	TaskRepository := repositories.NewTaskRepository(PgClient.Client)

	TaskCategoryRepository := repositories.NewTaskCategoryRepository(PgClient.Client)

	CategoryRepository := repositories.NewCategoryRepository(PgClient.Client)

	TaskService := service.NewTaskService(TaskRepository, TaskCategoryRepository, CategoryRepository)

	TaskController := controller.NewTaskController(TaskService, PgClient.Client)

	return NewContainer(PgClient.Client, UserRepository, AuthService, AuthController, AuthMiddleware, TokenService, TaskController)
}

func ShutdownContainer(container *Container) {
	// container.PgClient.Close()
}
