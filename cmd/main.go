package main

import (
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

func main() {
	godotenv.Load()
	container := InitializeContainer()

	defer ShutdownContainer(container)

	echo := echo.New()

	RegisterRouter(echo, container)

	SetupServer(echo)
}
